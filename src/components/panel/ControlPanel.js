import axios from 'axios'

export default {
  name: 'control-panel',
  data () {
    return {
      header: 'Post Trucks',
      filterForSearch: {
        origin: '',
        destination: '',
        weight: '',
        startDate: '',
        finishDate: ''
      },
      isShowRow: false
    }
  },
  methods: {
    async play () {
      let response
      try {
        let url = 'http://localhost:8080/trucks/all'
        response = await axios.get(url)
      } catch (error) {
        alert(error)
      }
      let list = response.data
      this.$parent.updateResults(list)
    },
    async newTrucks () {
      let url = 'http://localhost:8080/trucks/save'
      let dataPost = JSON.stringify({
        'origin': this.filterForSearch.origin,
        'destination': this.filterForSearch.destination,
        'weight': this.filterForSearch.weight,
        'startDate': this.filterForSearch.startDate,
        'finishDate': this.filterForSearch.finishDate
      })
      let config = {
        headers: {
          'Content-Type': 'application/json'
        }
      }
      try {
        await axios.post(url, dataPost, config)
        this.cleanFilterForSearch()
      } catch (error) {
        alert(error)
      }
      this.hideRow()
    },
    showRow () {
      this.isShowRow = !this.isShowRow
    },
    hideRow () {
      this.isShowRow = false
    },
    cleanFilterForSearch () {
      this.filterForSearch = {
        origin: '',
        destination: '',
        weight: '',
        startDate: '',
        finishDate: ''
      }
    }
  }
}
