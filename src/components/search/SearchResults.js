import axios from 'axios'

export default {
  name: 'search-results',
  data () {
    return {
      header: 'Search Results',
      results: []
    }
  },
  methods: {
    showListTrucks (list) {
      this.results = list
    },
    async deleteTruck (id) {
      let url = 'http://localhost:8080/trucks/delete/' + id
      try {
        await axios.delete(url)
        this.results = this.results.filter(function (item) {
          return item.id !== id
        })
      } catch (error) {
        alert(error)
      }
    }
  }
}
