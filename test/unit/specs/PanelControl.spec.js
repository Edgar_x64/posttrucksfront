import Vue from 'vue'
import ControlPanel from '@/components/panel/ControlPanel.vue'

describe('ControlPanel.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(ControlPanel)
    const vm = new Constructor().$mount()
    expect(vm.$el.getElementsByTagName('h3').item(0).textContent)
      .to.equal('Post Trucks')
  })
})
