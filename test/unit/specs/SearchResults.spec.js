import Vue from 'vue'
import SearchResults from '@/components/search/SearchResults.vue'

describe('SearchResults.vue', () => {
  it('should render correct contents', () => {
    const Constructor = Vue.extend(SearchResults)
    const vm = new Constructor().$mount()
    expect(vm.$el.getElementsByTagName('h1').item(0).textContent)
      .to.equal('Search Results')
  })
})
